package com.github.wxiaoqi.rest;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tk.mybatis.mapper.entity.Example;

import com.github.wxiaoqi.biz.TestBiz;
import com.github.wxiaoqi.entity.Test;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

@Controller
@RequestMapping("test")
public class TestController extends BaseController<TestBiz, Test>{
	  @Autowired
	  private TestBiz testBiz;
	  
	  @RequestMapping(value = "/list", method = RequestMethod.GET)
	  @ResponseBody
	  public TableResultResponse<Test> page(@RequestParam(defaultValue = "10") int limit,
	      @RequestParam(defaultValue = "1") int offset,String name, @RequestParam(defaultValue = "0") int menuId) {
	    Example example = new Example(Test.class);
	    Example.Criteria criteria = example.createCriteria();
	    /*criteria.andEqualTo("menuId", menuId);
	    if(StringUtils.isNotBlank(name)){
	      criteria.andLike("name", "%" + name + "%");
	    }*/
	    List<Test> elements = testBiz.selectByExample(example);
	    return new TableResultResponse<Test>(elements.size(), elements);
	  }
}
