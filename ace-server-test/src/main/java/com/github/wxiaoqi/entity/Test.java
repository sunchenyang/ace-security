package com.github.wxiaoqi.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;

import lombok.Data;
@Data
@Table(name = "test")
public class Test extends BaseEntity{
	@Column(name = "test")
    private String test;
}
