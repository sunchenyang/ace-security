package com.github.wxiaoqi.biz;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ace.cache.annotation.Cache;
import com.ace.cache.annotation.CacheClear;
import com.github.wxiaoqi.entity.Test;
import com.github.wxiaoqi.mapper.TestMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

@Service
@Transactional(rollbackFor = Exception.class)
public class TestBiz extends BaseBiz<TestMapper, Test> {
	@Cache(key = "permission:test")
	public List<Test> selectAllElementPermissions() {
		return mapper.selectAllElementPermissions();
	}

	@Override
	@CacheClear(keys = { "permission:test", "permission" })
	public void insertSelective(Test test) {
		super.insertSelective(test);
	}

	@Override
	@CacheClear(keys = { "permission:test", "permission" })
	public void updateSelectiveById(Test entity) {
		super.updateSelectiveById(entity);
	}
}
