package com.github.wxiaoqi.mapper;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;

import com.github.wxiaoqi.entity.Test;

public interface TestMapper extends Mapper<Test>{
	
	  public List<Test> selectAllElementPermissions();
	  
}
